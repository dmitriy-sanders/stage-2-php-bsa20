<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    /**
     * Rendering data about cars on track to HTML.
     * PHP_EOL for readable view.
     *
     * @param Track $track
     * @return string
     */
    public function present(Track $track): string
    {
        $cars = $track->all();
        $contentBlock = "<div class='container'>" . PHP_EOL . "<h1>Cars on Track</h1>". PHP_EOL;

        foreach ($cars as $car) {
            $carBlock = '<div class="car">' . PHP_EOL;

            $carBlock .= '<img src="'. $car->getImage() .'">';

            $carBlock .= PHP_EOL . "<p><b>Name: MaxSpeed, FuelTank Volume</b></p>" . PHP_EOL;
            $carBlock .= "<p>" . $car->getName() . ": "
                . $car->getSpeed() . ", "
                . $car->getFuelTankVolume()
                . "</p>" . PHP_EOL;

            $carBlock .= "<p>Fuel Consumption: " .
                $car->getFuelConsumption() . " litres per 100 km "
                . "</p>" . PHP_EOL;

            $carBlock .= "</div>" . PHP_EOL . PHP_EOL;

            $contentBlock .= $carBlock;
        }
        $contentBlock .= "</div>";

        if (empty($cars)) {
            $contentBlock = "<h1>Track is empty!</h1>";
        }
        return $contentBlock;
    }
}
