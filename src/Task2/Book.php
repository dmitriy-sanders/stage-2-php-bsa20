<?php

declare(strict_types=1);

namespace App\Task2;

use lib\Validator;

class Book
{
    protected string $title;
    protected int $price;
    protected int $pages;

    /**
     * Initializing book characteristics.
     *
     * @param string $title
     * @param int $price
     * @param int $pages
     */
    public function __construct(
        string $title,
        int $price,
        int $pages
    ) {
        $this->title = Validator::validateString($title, 'Title');
        $this->price = Validator::validateNumber($price, 'Price');
        $this->pages = Validator::validateNumber($pages, 'Number Pages');
    }

    /**
     * Get book title.
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Get book price.
     *
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * Get book pages number.
     *
     * @return int
     */
    public function getPagesNumber(): int
    {
        return $this->pages;
    }
}
