<?php

declare(strict_types=1);

namespace App\Task1;

use lib\Validator;

class Car
{
    protected int $id;
    protected string $image;
    protected string $name;
    protected int $speed;
    protected int $pitStopTime;
    protected float $fuelConsumption;
    protected float $fuelTankVolume;

    /**
     * Car constructor. Initialize car characteristics.
     * @param int $id
     * @param string $image
     * @param string $name
     * @param int $speed
     * @param int $pitStopTime
     * @param float $fuelConsumption
     * @param float $fuelTankVolume
     */
    public function __construct(
        int $id,
        string $image,
        string $name,
        int $speed,
        int $pitStopTime,
        float $fuelConsumption,
        float $fuelTankVolume
    ) {
        $this->id              = Validator::validateNumber($id, 'Id');
        $this->image           = Validator::validateString($image, 'Image');
        $this->name            = Validator::validateString($name, 'Name');
        $this->speed           = Validator::validateNumber($speed, 'Speed');
        $this->pitStopTime     = Validator::validateNumber($pitStopTime, 'PitStopTime');
        $this->fuelConsumption = Validator::validateNumber($fuelConsumption, 'FuelConsumption');
        $this->fuelTankVolume  = Validator::validateNumber($fuelTankVolume, 'FuelTankVolume');
    }

    /**
     * Get car id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get car image.
     *
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * Get car name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get car speed.
     * In kilometers per hour.
     *
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * Get pitStop time.
     * In seconds.
     *
     * @return int
     */
    public function getPitStopTime(): int
    {
        return $this->pitStopTime;
    }

    /**
     * Get fuel consumption.
     * In litres per 100 kilometers.
     *
     *@return float
     */
    public function getFuelConsumption(): float
    {
        return $this->fuelConsumption;
    }

    /**
     * Get Fuel Tank Volume.
     * In litres.
     *
     * @return float
     */
    public function getFuelTankVolume(): float
    {
        return $this->fuelTankVolume;
    }
}
