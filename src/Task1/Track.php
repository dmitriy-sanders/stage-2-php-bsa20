<?php

declare(strict_types=1);

namespace App\Task1;

use lib\Exceptions\NoCarExistsException;

class Track
{
    protected array $cars = [];
    protected float $lapLength;
    protected int $lapsNumber;
    protected float $totalWay;
    protected int $kmPerOneConsumption = 100;

    /**
     * Track constructor. Initialize track params.
     * @param float $lapLength
     * @param int $lapsNumber
     */
    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;

        $this->totalWay = $lapLength * $lapsNumber;
    }

    /**
     * Get Lap Length.
     * @return float
     */
    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    /**
     * Get Laps Number.
     * @return int
     */
    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    /**
     * Add car on the track.
     * @param Car $car
     * @return void
     */
    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    /**
     * Get all cars, which on the tracks.
     * @return array
     */
    public function all(): array
    {
        if (empty($this->cars)) {
            throw new NoCarExistsException("There are no cars on the track!");
        }
        return $this->cars;
    }

    /**
     * Choose the winner's car, depending on characteristics.
     *
     * @return Car
     * @var Car[] $cars
     */
    public function run(): Car
    {
        $cars = $this->all();
        $carsWithTime = [];

        foreach ($cars as $key => $car) {
            $speed = $car->getSpeed();
            $fuelTankVolume = $car->getFuelTankVolume();
            $fuelConsumption = $car->getFuelConsumption();
            $pitStopTime = $car->getPitStopTime();

            $secondsOnTheTrack = $this->getSecondsOnTrack($speed);
            $pitStopsAmount = $this->getPitStopsAmount($fuelConsumption, $fuelTankVolume);
            $secondsOnPitStop = $this->getSecondsOnPitStop($pitStopsAmount, $pitStopTime);

            $totalSpentSeconds = $secondsOnTheTrack + $secondsOnPitStop;
            $carsWithTime[$key] = $totalSpentSeconds;
        }

        return $this->all()[array_search(min($carsWithTime), $carsWithTime)];
    }

    /**
     * Get time on the track in seconds.
     *
     * @param $speed
     * @return float
     */
    public function getSecondsOnTrack($speed): float
    {
        $timePerOneConsumptionDistance = $this->kmPerOneConsumption / $speed;
        return ($this->totalWay / $this->kmPerOneConsumption) * $timePerOneConsumptionDistance * 60 * 60;
    }

    /**
     * Get Pit Stops amount for all run.
     *
     * @param $fuelConsumption
     * @param $fuelTankVolume
     * @return float
     */
    public function getPitStopsAmount($fuelConsumption, $fuelTankVolume): float
    {
        return floor((($this->totalWay / $this->kmPerOneConsumption) * $fuelConsumption) / $fuelTankVolume);
    }

    /**
     * Get time in seconds for all Pit Stops.
     *
     * @param $pitStopsAmount
     * @param $pitStopTime
     * @return float
     */
    public function getSecondsOnPitStop($pitStopsAmount, $pitStopTime): float
    {
        return $pitStopsAmount * $pitStopTime;
    }

}
