<?php
/**
 * Description for functions
 *
 * @author Dmitriy Uvin
 * @see https://dmitriy-sanders.github.io/homepage
 */

/**
 * Debug the data in readable form.
 *
 * @param mixed $data
 */
function dp($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}
