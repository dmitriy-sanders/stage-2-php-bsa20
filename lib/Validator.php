<?php

declare(strict_types=1);

namespace lib;

use lib\Exceptions\NumberException;
use lib\Exceptions\StringException;

class Validator
{
    /**
     * @param $number
     * @param string $numberName
     * @return mixed
     * @throws \Exception
     */
    public static function validateNumber($number, $numberName = 'Value')
    {
        if ($number <= 0) {
            throw new NumberException("$numberName can't be less or equal zero!");
        }
        return $number;
    }

    /**
     * @param string $string
     * @param string $stringName
     * @return string
     * @throws \Exception
     */
    public static function validateString(string $string, $stringName = 'This string')
    {
        if (empty($string)) {
            throw new StringException("$stringName can't be empty!");
        }
        return $string;
    }
}